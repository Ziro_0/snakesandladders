import { Component } from '@angular/core';
import { Platform, MenuController, NavController, NavParams } from 'ionic-angular';
import { Game as GameSnakes } from '../../game-snakes-and-ladders/game';
import { JsonKey } from '../../game-snakes-and-ladders/classes/data/JsonKey';
import PlayerData from '../../game-snakes-and-ladders/classes/data/PlayerData';
import IHomeServer from '../../game-snakes-and-ladders/interfaces/IHomeServer';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage implements IHomeServer {
    showAlertMessage: boolean = true;
    autoplayTimer: any = null;
    isDemo: boolean = true;
    isIos: boolean = false;
    isTutorial: boolean = false;
    finalStageNumber: number = 7;
    tutorialStage: number = 1;
    visibleState: string = 'visible';
    randomLeft: number = 0;
    opponentPoints: number = 0;
    opponentPointsLoading: boolean = true;
    bottomPosition: string = '-100px';
    staticTimerFn: any = null;
    internetAvailable: boolean = true;
    isBumper: boolean = false;
    bumperType: any = 1;
    problemText: string = null;
    loadingController: any = null;
    singleNav: any = null;
    resultsArray: any = {
        rightbubbles: 0,
        wrongbubbles: 0,
        rightTreats: 0,
        wrongTreats: 0,
        totalbubbles: 0
    };
    livesRemaining: boolean = false;
    resultDeclared: boolean = false;
    persistantLives: number = 10;
    perishableLives: number = 5;

    //Change bubble bottom position
    changeBottomIndex: number = 0;

    bubbles: any = [];
    bubblesSorted: any = [];
    declaredResultData: any = {};
    popIndex: number = 0;

    gamingIndex: number = 0;

    //Timer seconds
    timerNum: number = 60;
    points: any = 0;
    bumperPoints: number = 0;
    onEnded: boolean = false;
    onLeft: boolean = false;
    problemsOver: boolean = false;
    chosenColor: string = 'red';
    bubblePopRightIndex = 0;
    bubblePopWrongIndex = 0;
    level = 0;
    boxWidth = 0;
    boxHeight = 0;
    bubbleWidthParameter = 0;
    tableMoney = 5;
    levelIncreasing: boolean = true;
    freshlyLoaded: boolean = true;
    showLoading: boolean = false;

    staticTimer: number = 2;
    staticTimerBubbleFn: any;

    //For demo
    config: any = {};
    levelConfig: any = {
    };

    gameInstance: GameSnakes;
    gameStyle: any = {
        width: '100%',
        height: '100%'
    };
    paramsGameId: any;

    //=========================================================================
    // snakes-specific class variables

    /** config data block designed to be read-only */
    snakesConfig: any = {
        /** 
         * @type {number}
         * number of available avataras available
         */
        numberOfAvailableAvatars: 6,

        /** 
         * @type {string[]}
         * color keys of all avatars available to each player
         */
        playerTokenColors: [ 'blue', 'pink', 'red', 'yellow' ],

        /** array of data blocks for setting up each player */
        players: [
            {
                /**
                 * name of the player
                 * @type {string}
                 */
                name: 'Player 1',

                /**
                 * color of the player's token. you can specify one of the values
                 * from the `playerTokenColors` object. If left `undefined`, a random
                 * color will be chosen
                 * @type {string}
                 */
                color: undefined,

                /**
                 * zero-based tile index on which the player starts on. The tile index
                 * is relative to the number printed on the board, minus one.
                 * Used for testing.
                 * Set to 0 for production.
                 */
                startIndex: 0,

                /**
                 * zero-based index that references the avatar represented for this player
                 * if left `undefined`, a random avatar will be chosen
                 */
                avatarIndex: undefined
            },
            
            {
                name: 'Player 2',
                color: undefined,
                startIndex: 0,
                avatarIndex: undefined
            }
        ],

        /**
         * time, in ms, of each player's turn. if the time expires, the turn
         * player's dice will auto-roll
         */
        playerTimePerTurn: 10 * 1000,

        /**
         * determines if snakes will be used. set to `false` for production
         */
        debugDisableSnakes: false,

        /**
         * determines if ladders will be used. set to `false` for production
         */
        debugDisableLadders: false
    };

    /** index of the current turn player */
    turnPlayerIndex: number;

    /** player data used in-game. initialized bu the data in snakesConfig.players */
    _playersData: PlayerData[];

    _playerTurnTimer: Phaser.Timer;

    /** decides what the next dice roll value will be */
    _nextDiceRollValue: number;

    _wouldPlayerMovePastExit: boolean;

    //=========================================================================

    constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public menu: MenuController) {
        this.paramsGameId = navParams.get('paramsGameId');
    }

    ionViewDidLoad() {
        this.calculateBoxWidth();
        this.startNow(this.snakesConfig.startTimeSecs);
    }

    perfectCallback(gameInstance) {
        console.log('perfectCallback called in home.ts!');
        gameInstance.showPerfect();
    }

    addPointCallback(points) {
       console.log('Adding a point', points);
       this.points = this.points + points;
    }

    //=========================================================================
    //---- BEGIN SNAKES AND LADDERS-SPECIFIC GAME LOGIC
    //=========================================================================

    //-------------------------------------------------------------------------
    get audioJsonData(): any {
        return(this.gameInstance.cache.getJSON(JsonKey.AUDIO));
    }

    //-------------------------------------------------------------------------
    getPlayerDataAt(index: number): PlayerData {
        return(this._playersData[index]);
    }

    //-------------------------------------------------------------------------
    get playersData(): PlayerData[] {
        return(this._playersData);
    }

    //-------------------------------------------------------------------------
    snakesEngineListener() {
        this._initSnakesGameServerEnv();

        this.gameInstance = new GameSnakes(this.boxWidth, this.boxHeight, this);
        this.gameInstance.startGame(this.snakesConfig);

        this.gameInstance.listen('dicePressed', this._onDicePressed.bind(this));
        this.gameInstance.listen('playerMoveTokenComplete',
            this._onPlayerMoveTokenComplete.bind(this));
        this.gameInstance.listen('gameClosed', this._onGameClosed.bind(this));
        this.gameInstance.listen('gameStart', this._onGameStart.bind(this));
        this.gameInstance.listen('ready', this._onReady.bind(this));
    }

    //-------------------------------------------------------------------------
    get turnPlayerData(): PlayerData {
        return(this.getPlayerDataAt(this.turnPlayerIndex));
    }

    //-------------------------------------------------------------------------
    _buildTurnPlayerTileIndexMoveOrder() {
        const jMoveTileOrders: number[] = this._getDataFromJsonChain(
            JsonKey.METRICS,
            ['board', 'moveTileOrders']);
        const numTiles = jMoveTileOrders.length;

        const turnPlayerData = this.turnPlayerData;
        const tileIndexMoveOrder = turnPlayerData.tileIndexMoveOrder;
        tileIndexMoveOrder.length = 0;
        turnPlayerData.usedLadder = false;
        turnPlayerData.usedSnake = false;
    
        let isReversing: boolean = false;
        let revStep: number;

        for (let step = 0; step < this._nextDiceRollValue; step++) {
            let playerTileIndex: number;
            
            if (!isReversing) {
                playerTileIndex = turnPlayerData.tileIndex + step + 1;
                if (playerTileIndex === numTiles) {
                    isReversing = true;
                    revStep = 1;
                }
            }

            if (isReversing) {
                playerTileIndex = (numTiles - 1) - revStep;
                revStep++;
            }

            tileIndexMoveOrder.push(playerTileIndex);
        }

        if (!this._checkForLadderWarp()) {
            this._checkForSnakeWarp();
        }

        const lastTileIndex = tileIndexMoveOrder[tileIndexMoveOrder.length - 1];
        turnPlayerData.tileIndex = lastTileIndex;

        this._checkForTurnPlayerWin();
    }

    //-------------------------------------------------------------------------
    _checkForBoardWarp(tileIndexMappings: number[][]): boolean {
        const turnPlayerData = this.turnPlayerData;
        const tileIndexMoveOrder = turnPlayerData.tileIndexMoveOrder;
        const lastTileIndex = tileIndexMoveOrder[tileIndexMoveOrder.length - 1];

        for (let index = 0; index < tileIndexMappings.length; index++) {
            const toFromTileMapping = tileIndexMappings[index];
            if (lastTileIndex === toFromTileMapping[0]) {
                tileIndexMoveOrder.push(toFromTileMapping[1]);
                return(true);
            }
        };

        return(false);
    }

    //-------------------------------------------------------------------------
    _checkForLadderWarp(): boolean {
        if (this.snakesConfig.debugDisableLadders) {
            return(false);
        }

        const usedWarp = this._checkForBoardWarp(this._getDataFromJsonChain(
            JsonKey.METRICS,
            ['board', 'ladders']));

        if (usedWarp) {
            const turnPlayerData = this.turnPlayerData;
            turnPlayerData.usedLadder = true;
            turnPlayerData.usedSnake = false;
        }

        return(usedWarp);
    }

    //-------------------------------------------------------------------------
    _checkForSnakeWarp(): boolean {
        if (this.snakesConfig.debugDisableSnakes) {
            return(false);
        }
        
        const usedWarp = this._checkForBoardWarp(this._getDataFromJsonChain(
            JsonKey.METRICS,
            ['board', 'snakes']));
        
        if (usedWarp) {
            const turnPlayerData = this.turnPlayerData;
            turnPlayerData.usedSnake = true;
            turnPlayerData.usedLadder = false;
        }

        return(usedWarp);
    }

    //-------------------------------------------------------------------------
    _checkForTurnPlayerWin(): boolean {
        const jMoveTileOrders: number[] = this._getDataFromJsonChain(
            JsonKey.METRICS,
            ['board', 'moveTileOrders']);
        
        const turnPlayerData = this.turnPlayerData;
        
        turnPlayerData.isWinner =
            turnPlayerData.tileIndex === jMoveTileOrders.length - 1;
        return(turnPlayerData.isWinner);
    }

    //-------------------------------------------------------------------------
    _getDataFromJsonChain(jsonCacheKey: JsonKey,
    propertyNameChains: string[]): any {
        let jData = this.gameInstance.cache.getJSON(jsonCacheKey);
        if (!jData || !propertyNameChains) {
            return(null);
        }

        for (let index = 0; index < propertyNameChains.length; index++) {
            const propName = propertyNameChains[index];
            if (propName in jData) {
                jData = jData[propName];
            } else {
                return(null);
            }
        }

        return(jData);
    }

    //-------------------------------------------------------------------------
    _handleDicePressed(playerIndex: number) {
        this._playerTurnTimer.stop();

        if (playerIndex !== this.turnPlayerIndex) {
            //server validation check
            console.warn(`the specified player index '${playerIndex}' doesn't ` +
                `match the current turn player index '${this.turnPlayerIndex}'`);
            this._setNextTurnPlayer();
            return;
        }

        const playerData = this._playersData[playerIndex];
        if (!playerData) {
            //sanity check of data coming from client
            console.log('invalid player index: ' + playerIndex);
            return;
        }

        if (this._wouldPlayerMovePastExit) {
            return;
        }

        this._buildTurnPlayerTileIndexMoveOrder();
        const tileIndexMoveOrder = playerData.tileIndexMoveOrder;
        playerData.tileIndex = tileIndexMoveOrder[tileIndexMoveOrder.length - 1];
    }

    //-------------------------------------------------------------------------
    _checkIfPlayerWouldMovePastExit(playerIndex: number): boolean {
        const playerData: PlayerData = this.getPlayerDataAt(playerIndex);
        const jMoveTileOrders: number[] = this._getDataFromJsonChain(
            JsonKey.METRICS,
            ['board', 'moveTileOrders']);
        
        return(playerData.tileIndex + this._nextDiceRollValue >= jMoveTileOrders.length);
    }

    //-------------------------------------------------------------------------
    _initPlayersData() {
        this._playersData = [];
        
        const snakesConfig = this.snakesConfig;
        const playersConfigs: any[] = snakesConfig.players;
        const availablePlayerColors: string[] = snakesConfig.playerTokenColors.concat();

        playersConfigs.forEach(function(playerConfig) {
            const playerColor = this._resolvePlayerColor(playerConfig.color,
                availablePlayerColors);
            const avatarIndex = this._resolvePlayerAvatarIndex(playerConfig.avatarIndex);
            const playerTileIndex =
                this._resolvePlayerStartTileIndex(playerConfig.startIndex);

            const playerData = new PlayerData(
                playerConfig.name,
                playerColor,
                avatarIndex,
                playerTileIndex);

            this._playersData.push(playerData);
        }, this);
    }

    //-------------------------------------------------------------------------
    _initSnakesGameServerEnv() {
        this._initPlayersData();
    }

    //-------------------------------------------------------------------------
    _onDicePressed(gameInstance: GameSnakes, playerIndex: number) {
        console.log('dice pressed for player: ' + playerIndex);
        this._handleDicePressed(playerIndex);
    }
    
    //-------------------------------------------------------------------------
    _onEndForSnakes() {
        if (this._playerTurnTimer) {
            this._playerTurnTimer.destroy();
            this._playerTurnTimer = null;
        }
    }

    //-------------------------------------------------------------------------
    _onGameClosed() {
        this.onEnd();
    }

    //-------------------------------------------------------------------------
    _onGameOver(gameInstance: GameSnakes) {
        console.log('game over callback!');
    }

    //-------------------------------------------------------------------------
    _onGameStart(gameInstance: GameSnakes) {
        console.log('game start callback!');
    }

    //-------------------------------------------------------------------------
    _onPlayerMoveTokenComplete() {
        const turnPlayerData = this.turnPlayerData;
        if (turnPlayerData.isWinner) {
            console.log('player winner: ' + this.turnPlayerIndex);
            this.gameInstance.turnPlayerWins();
        } else {
            const CONTINUE_TURN_PLAYER_DICE_VALUE = 6;
            if (this._nextDiceRollValue === CONTINUE_TURN_PLAYER_DICE_VALUE) {
                this._setTurnPlayer(this.turnPlayerIndex);
            } else {
                this._setNextTurnPlayer();
            }
        }
    }

    //-------------------------------------------------------------------------
    _onReady(gameInstance: GameSnakes) {
        console.log('the game has finished loading and is ready to play');
        const IS_FIRST_TURN_OF_GAME = true;
        this._setTurnPlayer(0, IS_FIRST_TURN_OF_GAME);
    }

    //-------------------------------------------------------------------------
    _onPlayerTurnTimerComplete() {
        console.log('turn player\'s time expired');
        this._handleDicePressed(this.turnPlayerIndex);
        this.gameInstance.turnPlayerTimeExpired();
    }

    //-------------------------------------------------------------------------
    _resolvePlayerAvatarIndex(avatarIndex: number): number {
        return(typeof avatarIndex === 'number' ?
            avatarIndex :
            Math.floor(Math.random() * this.snakesConfig.numberOfAvailableAvatars));
    }

    //-------------------------------------------------------------------------
    _resolvePlayerColor(color: string, availablePlayerColors_in_out: string[]): string {
        return(typeof color === 'string' ?
            color :
            Phaser.ArrayUtils.removeRandomItem(availablePlayerColors_in_out));
    }

    //-------------------------------------------------------------------------
    _resolvePlayerStartTileIndex(startIndex: number): number {
        return(typeof startIndex === 'number' ? startIndex : 0);
    }

    //-------------------------------------------------------------------------
    _setNextTurnPlayer() {
        const nextPlayerIndex = (this.turnPlayerIndex + 1) % this._playersData.length;
        this._setTurnPlayer(nextPlayerIndex);
    }

    //-------------------------------------------------------------------------
    _setTurnPlayer(playerIndex: number, isFirstTurnOfGame: boolean = false) {
        const gameState = this.gameInstance.currentGameState;
        if (!gameState) {
            console.warn('the GameState must be active first!');
            return;
        }

        this.turnPlayerIndex = playerIndex;

        const MIN_DICE_VALUE = 1;
        const MAX_DICE_VALUE = 6;
        this._nextDiceRollValue = MIN_DICE_VALUE + Math.floor(Math.random() * MAX_DICE_VALUE);
        
        this._wouldPlayerMovePastExit =
            this._checkIfPlayerWouldMovePastExit(this.turnPlayerIndex);
        
        gameState.setTurnPlayer(this.turnPlayerIndex, this._nextDiceRollValue,
            isFirstTurnOfGame, this._wouldPlayerMovePastExit);
        this._startPlayerTurnTimer();
    }

    //-------------------------------------------------------------------------
    _startPlayerTurnTimer() {
        const AUTO_DESTROY = false;
        this._playerTurnTimer = this._playerTurnTimer ||
            this.gameInstance.time.create(AUTO_DESTROY);
        this._playerTurnTimer.start();

        this._playerTurnTimer.add(this.snakesConfig.playerTimePerTurn,
            this._onPlayerTurnTimerComplete, this);
    }

    //=========================================================================
    //---- END SNAKES AND LADDERS-SPECIC GAME LOGIC
    //=========================================================================

    startNow(num) {
        this.gameStyle = {
            width: this.boxWidth + 'px',
            height: this.boxHeight + 'px'
        };

        switch (this.paramsGameId) {
            case 19:
                this.snakesEngineListener();
                break;
        }

        //Information to be used!
        this.showAlertMessage = false;
        this.onEnded = false;
        this.onLeft = false;

        this.timerNum = num; //Value of the timer
        this.perishableLives = 5; //Initial per contest lives
        this.persistantLives = 0; //Final bought lives

        this.points = 0; //Points scored

        this.checkLivesAvailable(); //This function checks if lives are available or not! It ends your contest if your lives are over.
    }

    checkLivesAvailable() {
        if (this.perishableLives <= 0 && this.persistantLives <= 0) {
            this.livesRemaining = false;
            this.onEnd();
        } else {
            this.livesRemaining = true;
        }
    }

    calculateBoxWidth() {
        this.boxWidth = this.platform.width();
        this.boxHeight = this.platform.height();

        // if (this.isIos) {
        //     this.boxHeight = this.boxHeight - 50;
        // }
    }

    problemsOverFn(problemText?) {
        this.problemText = 'Some problem occurred, Please try again!';
        if (problemText) {
            this.problemText = problemText;
        }
        this.timerNum = 0;
        this.problemsOver = true;
        this.clearTimer();
    }

    nextStage() {
        this.tutorialStage = this.tutorialStage + 1;

        if (this.tutorialStage == 4) {
            this.perishableLives = 0;
        }
        if (this.tutorialStage == 5) {
            this.persistantLives = 0;
        }

        if (this.tutorialStage == this.finalStageNumber + 1) {
            this.dismiss();
        }
    }

    chooseLevel() {
        if (this.level < this.config.levels + 1 && this.levelIncreasing) {
            this.level++;
        }

        if (this.level > 0 && !this.levelIncreasing) {
            this.level--;
        }

        if (this.level == 0) {
            this.level = 2;
            this.levelIncreasing = true;
        }

        if (this.level == this.config.levels + 1) {
            this.level = this.config.levels - 1;
            this.levelIncreasing = false;
        }

        this.levelConfig = this.config[this.level];
    }

    generateRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    dismiss() {
        this.navCtrl.pop();
    }

    confirmDismissal() {
        this.showAlertMessage = false;
        this.onEnded = true;
        this.onLeft = true;
        this.clearTimer();

        setTimeout(() => {
            this.clearTimer();
        }, 1000);

        this.menu.enable(true);
    }

    clearTimer() {
        if (this.staticTimerFn) {
            clearTimeout(this.staticTimerFn);
            this.staticTimerFn = null;
        }
    }

    ionViewDidEnter() {
        this.menu.enable(false);
    }

    ionViewCanLeave() {
        if (!this.onEnded && !this.problemsOver && !this.isTutorial) {
        } else {
            this.menu.enable(true);
        }
    }

    getBubbleParamsAccToWidth() {
        let width = this.boxWidth; //width of box
        let singleBoxParams = {};
        this.levelConfig.box.forEach(singleBox => {
            if (singleBox.minWidth) {
                if (singleBox.maxWidth) {
                    //Check width between minWidth and maxWidth
                    if (width >= singleBox.minWidth && width <= singleBox.maxWidth) {
                        singleBoxParams = singleBox;
                    }
                } else {
                    //Check width >= minWidth
                    if (width >= singleBox.minWidth) {
                        singleBoxParams = singleBox;
                    }
                }
            } else {
                //Check width between 0 and maxWidth
                if (width >= 0 && width <= singleBox.maxWidth) {
                    singleBoxParams = singleBox;
                }
            }
        });

        return singleBoxParams;
    }

    _arrayRandom(len, min, max, unique) {
        var len = len ? len : 10,
            min = min !== undefined ? min : 1,
            max = max !== undefined ? max : 100,
            unique = unique ? unique : false,
            toReturn = [],
            tempObj = {},
            i = 0;

        if (unique === true) {
            for (; i < len; i++) {
                var randomInt = Math.floor(Math.random() * (max - min + 1)) + min;
                if (tempObj['key_' + randomInt] === undefined) {
                    tempObj['key_' + randomInt] = randomInt;
                    toReturn.push(randomInt);
                } else {
                    i--;
                }
            }
        } else {
            for (; i < len; i++) {
                toReturn.push(Math.floor(Math.random() * (max - min + min)));
            }
        }

        return toReturn;
    }

    removeLoading() {
        if (this.showLoading) {
            this.showLoading = false;
            this.loadingController.dismiss();
        }
    }

    onTimerFinished() {
        this.onEnd();
    }

    onEnd() {
        /** for snakes */
        this._onEndForSnakes();
        
        if (!this.onEnded) {
            this.gameInstance.endGame();
        }
        this.clearTimer();
        this.onEnded = true;
        this.onLeft = true;

        if (this.isBumper || this.isDemo) {
            if (this.livesRemaining) {
                this.dismiss();
            }
        } else {
            if (this.livesRemaining) {
            }
        }
    }
}
