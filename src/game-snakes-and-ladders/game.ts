// import pixi, p2 and phaser ce
import 'pixi';
import 'p2';
import * as Phaser from 'phaser-ce';

// import states
import BootState from './states/BootState';
import PreloadState from './states/PreloadState';
import GameState from './states/GameState';
import IHomeServer from './interfaces/IHomeServer';

/**
 * Main entry game class
 * @export
 * @class Game
 * @extends {Phaser.Game}
 */
export class Game extends Phaser.Game {
    listenerMapping: any = {};
    homeServer: IHomeServer;
    config: any;

    //=========================================================================
    // constructor
    //=========================================================================
    /**
     * @constructor
     * Creates an instance of Game.
     * @memberof Game
     */
    constructor(width, height, homeServer: IHomeServer) {
        // call parent constructor
        // super(width, height, Phaser.CANVAS, 'game', null);
        super(540, 960, Phaser.CANVAS, 'game', null);

        this.homeServer = homeServer;
        console.log(width, height);

        // add some game states
        this.state.add('BootState', new BootState(this));
        this.state.add('PreloadState', new PreloadState(this));
        this.state.add('GameState', new GameState(this));
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    get currentGameState(): GameState {
        const state: any = this.state.getCurrentState();
        return(state && state.key === 'GameState' ? state : null);
    }

    //-------------------------------------------------------------------------
    endGame() {
        this.paused = true;
        setTimeout(() => {
            this.destroy();
        }, 100);
    }

    //-------------------------------------------------------------------------
    listen(listenValue: string, callback: Function) {
        this.listenerMapping[listenValue] = callback;
    }

    //-------------------------------------------------------------------------
    moveTurnPlayer() {
        this.currentGameState.moveTurnPlayer();
    }

    //-------------------------------------------------------------------------
    startGame(config: any) {
        console.log('game has started');
        
        this.config = config;
        
        console.log('config:');
        console.log(this.config);

        this.state.start('BootState');
    }

    //-------------------------------------------------------------------------
    turnPlayerTimeExpired() {
        const gameState = this.currentGameState;
        if (gameState) {
            gameState.turnPlayerTimeExpired();
        }
    }

    //-------------------------------------------------------------------------
    turnPlayerWins() {
        const gameState = this.currentGameState;
        if (gameState) {
            gameState.turnPlayerWins();
        }
    }

}
