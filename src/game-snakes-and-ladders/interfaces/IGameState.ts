import PlayerData from "../classes/data/PlayerData";
import { Game } from "../game";
import { AudioEventKey } from "../classes/audio/AudioEventKey";
import { JsonKey } from "../classes/data/JsonKey";
import PlayerUi from "../classes/ui/PlayerUi";

export default interface IGameState {
    audioJsonData: any;
    callListenerMap(key: string, ...args);
    game: Game;
    gatPlayerUi(playerIndex: number): PlayerUi;
    getPointFromJsonChain(jsonCacheKey: JsonKey, propertyNameChains: string[]): Phaser.Point;
    metricsJsonData: any;
    onPlayerDiceInputUp(playerIndex: number);
    onPlayerDiceRollComplete(playerIndex: number, rollValue: number);
    playersData: PlayerData[];
    playSound(key: AudioEventKey, callback?: Function, context?: any): boolean;
    turnPlayerIndex: number;
}
