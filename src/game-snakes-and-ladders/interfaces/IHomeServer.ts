import PlayerData from "../classes/data/PlayerData";

export default interface IHomeServer {
    playersData: PlayerData[];
    turnPlayerIndex: number;
}
