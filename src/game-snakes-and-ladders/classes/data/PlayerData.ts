export default class PlayerData {
    tileIndexMoveOrder: number[];

    name: string;
    tileIndex: number;

    isWinner: boolean;
    usedSnake: boolean;
    usedLadder: boolean;

    _avatarIndex: number
    _color: string;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(name: string, color: string, avatarIndex: number,
    tileIndex: number = 0) {
        this.name = name;
        this.tileIndex = tileIndex;
        this._avatarIndex = avatarIndex;
        this._color = color;

        this.tileIndexMoveOrder = [];
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    get avatarIndex(): number {
        return(this._avatarIndex);
    }

    //-------------------------------------------------------------------------
    get color(): string {
        return(this._color);
    }

    //=========================================================================
    // private
    //=========================================================================
}
