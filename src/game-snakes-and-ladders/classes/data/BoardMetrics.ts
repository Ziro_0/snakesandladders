import IGameState from "../../interfaces/IGameState";

export default class BoardMetrics {

    moveTileOrders: number[];

    cellSize: Phaser.Point;
    position: Phaser.Point;
    tokenStartOffset: Phaser.Point;

    numColumns: number;
    numRows: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState) {
        const jBoardData = gameState.metricsJsonData.board;

        const jCellSize = jBoardData.cellSize;
        this.cellSize = new Phaser.Point(jCellSize.width, jCellSize.height);

        const jGrid = jBoardData.grid;
        this.numColumns = jGrid.numColumns;
        this.numRows = jGrid.numRows;

        const jPosition = jBoardData.position;
        this.position = new Phaser.Point(jPosition.x, jPosition.y);

        const jTokenStartOffset = jBoardData.tokenStartOffset;
        this.tokenStartOffset = new Phaser.Point(jTokenStartOffset.x, jTokenStartOffset.y);

        this.moveTileOrders = jBoardData.moveTileOrders;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    calcCellPosition(playerTileIndex: number, position_out?: Phaser.Point): Phaser.Point {
        const mapTileIndex = this._convertPlayerTileIndexToMapTileIndex(playerTileIndex);
        if (mapTileIndex === undefined) {
            return(null);
        }

        if (!position_out) {
            position_out = new Phaser.Point();
        }

        const tileColumn = mapTileIndex % this.numColumns;
        const tileRow = Math.floor(mapTileIndex / this.numColumns);

        position_out.x = tileColumn * this.cellSize.x + this.tokenStartOffset.x;
        position_out.y = tileRow * this.cellSize.y + this.tokenStartOffset.y;

        return(position_out);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _convertPlayerTileIndexToMapTileIndex(playerTileIndex: number): number {
        return(this.moveTileOrders[playerTileIndex]);
    }
}
