import { Game } from "../game";
import TimerBar from "./ui/TimerBar";

export default class TurnTimer  {
    onTimerComplete: Phaser.Signal;

    _timerBar: TimerBar;

    _timer: Phaser.Timer;

    _maxDuration: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(game: Game) {
        this.onTimerComplete = new Phaser.Signal();

        const AUTO_DESTROY = false;
        this._timer = game.time.create(AUTO_DESTROY);
    }

    //=========================================================================
    // public
    //=========================================================================
    
    //-------------------------------------------------------------------------
    dispose() {
        this._timer.destroy();
        this.onTimerComplete.dispose();
    }

    //-------------------------------------------------------------------------
    get isRunning(): boolean {
        return(this._timer.running);
    }

    //-------------------------------------------------------------------------
    get paused(): boolean {
        return(this._timer.paused);
    }

    //-------------------------------------------------------------------------
    set paused(isPaused: boolean) {
        if (this._timer.running) {
            this._timer.paused = isPaused;
        }
    }

    //-------------------------------------------------------------------------
    start(duration: number, timerBar: TimerBar) {
        this._timerBar = timerBar;
        if (!this._timerBar) {
            return;
        }

        this._timerBar.visible = true;
        
        this._maxDuration = duration;
        this._timer.add(duration, this._onTimerComplete, this);
        this._timer.start();
    }

    //-------------------------------------------------------------------------
    stop() {
        if (this._timerBar) {
            this._timerBar.visible = false;
        }

        this._timer.paused = false;
        this._timer.stop();
    }

    //-------------------------------------------------------------------------
    update() {
        if (this._timerBar) {
            this._timerBar.value = this._timer.duration / this._maxDuration;
        }
    }

    //=========================================================================
    // private
    //=========================================================================
    
    //-------------------------------------------------------------------------
    _onTimerComplete() {
        this._timer.stop();
        this._timerBar.visible = false;

        if (this.onTimerComplete) {
            this.onTimerComplete.dispatch(this);
        }
    }
}
