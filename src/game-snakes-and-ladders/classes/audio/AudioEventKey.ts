export enum AudioEventKey {
    BUTTON_PRESS = 'buttonPress',
    DICE = 'dice',
    LADDER = 'ladder',
    NEXT_TURN = 'nextTurn',
    PLAYER_TIME_OVER = 'playerTimeOver',
    PLAYER_WINS = 'playerWins',
    SNAKE = 'snake',
    TOKEN_MOVE_STEP = 'tokenMoveStep'
}
