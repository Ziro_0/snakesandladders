import IGameState from "../../interfaces/IGameState";
import { JsonKey } from "../data/JsonKey";
import PlayerData from "../data/PlayerData";
import PlayerToken from "./PlayerToken";
import BoardMetrics from "../data/BoardMetrics";
import PlayerTokenMover from "./PlayerTokenMover";

export default class Board extends Phaser.Group {
    _playerTokens: PlayerToken[];

    _gameState: IGameState;
    
    _boardMetrics: BoardMetrics;
    _playerTokenMover: PlayerTokenMover;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, parent?: PIXI.DisplayObjectContainer) {
        super(gameState.game, parent);
        
        this._gameState = gameState;
        this._boardMetrics = new BoardMetrics(gameState);
        
        this._playerTokenMover = new PlayerTokenMover(gameState, this._boardMetrics);
        this._playerTokenMover.onComplete.add(this._onPlayerTokenMoverComplete, this);

        this._setPosition();

        this._initBoardImage();
        this._createtPlayerTokens();
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    destroy() {
        this._playerTokenMover.dispose();
        this._boardMetrics = null;
        this._gameState = null;
        super.destroy();
    }

    //-------------------------------------------------------------------------
    movePlayerToken(playerIndex) {
        const playerToken = this._playerTokens[playerIndex];
        if (!playerToken) {
            console.warn('no player token at index: ' + playerIndex);
            return;
        }

        const playerData = this._gameState.playersData[playerIndex];
        this._playerTokenMover.move(playerIndex, playerToken, playerData.tileIndexMoveOrder);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _initBoardImage() {
        this._gameState.game.add.image(0, 0, 'graphics', 'board', this);
    }

    //-------------------------------------------------------------------------
    _createPlayerToken(playerData: PlayerData) {
        const position = this._boardMetrics.calcCellPosition(playerData.tileIndex);
        const frame = `token-${playerData.color}`;
        const playerToken = new PlayerToken(this._gameState,
            position.x, 
            position.y, 
            frame);
        
        this.add(playerToken);
        this._playerTokens.push(playerToken);
    }

    //-------------------------------------------------------------------------
    _createtPlayerTokens() {
        this._playerTokens = [];

        this._gameState.game.homeServer.playersData.forEach(function(playerData) {
            this._createPlayerToken(playerData);
        }, this);
    }

    //-------------------------------------------------------------------------
    _onPlayerTokenMoverComplete(playerTokenMover: PlayerTokenMover,
    playerIndex: number) {
        const playerUi = this._gameState.gatPlayerUi(playerIndex);
        playerUi.isDiceVisible = false;

        this._gameState.callListenerMap('playerMoveTokenComplete');
    }

    //-------------------------------------------------------------------------
    _setPosition() {
        const boardPosition = this._gameState.getPointFromJsonChain(
            JsonKey.METRICS, [ 'board', 'position' ]);
        this.x = boardPosition.x
        this.y = boardPosition.y;
    }
}
