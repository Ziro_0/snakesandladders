import IGameState from "../../interfaces/IGameState";
import PlayerToken from "./PlayerToken";
import BoardMetrics from "../data/BoardMetrics";
import { AudioEventKey } from "../audio/AudioEventKey";

export default class PlayerTokenMover {
    /**
     * after a token has completed all of its moving steps, this value, specifies
     * how long the game should wait before setting the next player's turn
     */
    static readonly _MOVE_COMPLETE_DELAY_MS = 400;

    /**
     * specifies how long it takes a player's token to make a single step
     */
    static readonly _TOKEN_STEP_DURATION_MS = 100;

    /**
     * specifies how long a player's token should hold in between moving steps
     */
    static readonly _DELAY_IN_BETWEEN_STEPS_MS = 100;

    onComplete: Phaser.Signal;

    _tileIndexMoveOrder: number[];

    _gameState: IGameState;
    
    _playerToken: PlayerToken;
    _boardMetrics: BoardMetrics;

    _moveTimer: Phaser.Timer;
    _moveTween: Phaser.Tween;

    _tokenPosition: Phaser.Point;

    _playerIndex: number;
    _numSteps: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, boardMetrics: BoardMetrics) {
        this._gameState = gameState;
        this._boardMetrics = boardMetrics;

        this.onComplete = new Phaser.Signal();

        const AUTO_DESTROY = false;
        this._moveTimer = this._gameState.game.time.create(AUTO_DESTROY);
        this._moveTimer.start();

        this._playerIndex = -1;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    dispose() {
        this.onComplete.dispose();

        if (this._moveTimer) {
            this._moveTimer.destroy();
            this._moveTimer = null;
        }

        if (this._moveTween) {
            this._moveTween.stop();
            this._moveTween = null;
        }

        this._gameState = null;
        this._boardMetrics = null;
        this._playerToken = null;
    }

    //-------------------------------------------------------------------------
    move(playerIndex: number, playerToken: PlayerToken, tileIndexMoveOrder: number[]) {
        if (this._playerIndex > -1) {
            //can only move one token at a time
            return;
        }

        this._playerIndex = playerIndex;
        this._playerToken = playerToken;
        this._tileIndexMoveOrder = tileIndexMoveOrder.concat();
        this._nextStep();
    }

    //=========================================================================
    // _nextStep
    //=========================================================================

    //-------------------------------------------------------------------------
    _delayComplete() {
        this._moveTimer.add(PlayerTokenMover._MOVE_COMPLETE_DELAY_MS, function() {
            this.onComplete.dispatch(this, this._playerIndex);
            this._playerIndex = -1;
        }, this);
    }

    //-------------------------------------------------------------------------
    _nextStep() {
        const numSteps = this._tileIndexMoveOrder.length;
        if (numSteps === 0) {
            this._delayComplete();
            return;
        }

        this._playStepSound(numSteps);

        const playerTileIndex = this._tileIndexMoveOrder.shift();

        this._tokenPosition = this._boardMetrics.calcCellPosition(
            playerTileIndex,
            this._tokenPosition);

        this._moveTween = this._gameState.game.tweens.create(this._playerToken);
        this._moveTween.onComplete.add(this._onMoveTweenComplete, this);

        const AUTO_START = true;
        this._moveTween.to(
            {
                x: this._tokenPosition.x,
                y: this._tokenPosition.y
            },
            PlayerTokenMover._TOKEN_STEP_DURATION_MS,
            Phaser.Easing.Linear.None,
            AUTO_START);
    }

    //-------------------------------------------------------------------------
    _onMoveTimerComplete() {
        this._nextStep();
    }

    //-------------------------------------------------------------------------
    _onMoveTweenComplete() {
        this._moveTimer.add(PlayerTokenMover._DELAY_IN_BETWEEN_STEPS_MS,
            this._onMoveTimerComplete, this);
    }

    //-------------------------------------------------------------------------
    _playStepSound(numSteps: number) {
        if (numSteps === 1) {
            const playerData = this._gameState.playersData[this._playerIndex];
            if (playerData.usedLadder) {
                this._gameState.playSound(AudioEventKey.LADDER);
                return;
            } else if (playerData.usedSnake) {
                this._gameState.playSound(AudioEventKey.SNAKE);
                return;
            }
        }
        
        this._gameState.playSound(AudioEventKey.TOKEN_MOVE_STEP);
    }
}
