import { irandomRange } from '../../util/util';

export default class PlayerTurnIndicator extends Phaser.Image {
    _moveTween: Phaser.Tween;

    _startPosition: Phaser.Point;
    _endPosition: Phaser.Point;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(game: Phaser.Game, x: number, y: number, key: string,
    startPosition: Phaser.Point, endPosition: Phaser.Point, frame?: string) {
        super(game, x, y, key, frame);
        this.anchor.set(0.5);

        this._startPosition = startPosition;
        this._endPosition = endPosition;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    present() {
        if (this._moveTween) {
            return;
        }

        this.x = this._startPosition.x;
        this.y = this._startPosition.y;

        this._moveTween = this.game.add.tween(this);
        
        const DURATION = 400;
        const AUTO_START = true;
        const REPEAT = -1;
        const YOYO = true;
        this._moveTween.to(
            {
                x: this._endPosition.x,
                y: this._endPosition.y
            },
            DURATION,
            Phaser.Easing.Circular.In,
            AUTO_START,
            0,
            REPEAT,
            YOYO);

        this.visible = true;
    }

    //-------------------------------------------------------------------------
    unpresent() {
        this._removeTween();
        this.visible = false;
    }
    
    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _removeTween() {
        if (this._moveTween) {
            this._moveTween.stop();
            this._moveTween = null;
        }
    }
}
