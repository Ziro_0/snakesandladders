import Button from "./Button";
import IGameState from "../../interfaces/IGameState";
import Board from "./Board";
import PlayerUi from "./PlayerUi";
import PlayerWinnerPanel from "./PlayerWinnerPanel";
import { AudioEventKey } from "../audio/AudioEventKey";

export default class MainView {
    _playersUis: PlayerUi[];

    _gameState: IGameState;

    _board: Board;

    _buttonsGroup: Phaser.Group;
    _boardGroup: Phaser.Group;
    _uiGroup: Phaser.Group;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState) {
        this._gameState = gameState;

        this._initBackground();

        this._buttonsGroup = this._gameState.game.add.group();
        this._boardGroup = this._gameState.game.add.group();
        this._uiGroup = this._gameState.game.add.group();

        this._initBoard();
        this._initPlayersUis();
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    dispose() {
        this._buttonsGroup.destroy();
        this._boardGroup.destroy();
        this._uiGroup.destroy();
        this._board.destroy();
    }

    //-------------------------------------------------------------------------
    getPlayerUi(playerIndex: number): PlayerUi {
        return(this._playersUis[playerIndex]);
    }

    //-------------------------------------------------------------------------
    presentWinnerPanel() {
        const winnerPanel = new PlayerWinnerPanel(
            this._gameState,
            this._gameState.playersData[this._gameState.turnPlayerIndex],
            this._uiGroup);

        winnerPanel.x = this._gameState.game.world.centerX;
        winnerPanel.y = this._gameState.game.world.centerY;
    }

    //-------------------------------------------------------------------------
    turnPlayerTimeExpired() {
        const turnPlayerIndex = this._gameState.turnPlayerIndex;
        const playerUi = this.getPlayerUi(turnPlayerIndex);
        if (!playerUi) {
            return;
        }

        playerUi.unpresentTurnIndicator();
        playerUi.diceEnabled = false;
        playerUi.rollDice();

        this._gameState.playSound(AudioEventKey.DICE);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _initBackground() {
        const world = this._gameState.game.world;
        const backgroundImage = this._gameState.game.add.image(
            world.centerX, world.centerY,
            'graphics', 'background');

        backgroundImage.anchor.set(0.5);
        
        const jMetrics = this._gameState.metricsJsonData;
        backgroundImage.angle = jMetrics.backgroundImageAngle || 0;
        
        backgroundImage.width = world.width;
        if (backgroundImage.height < world.height) {
            backgroundImage.height = world.height;
            backgroundImage.scale.x = backgroundImage.scale.y;
        } else {
            backgroundImage.scale.y = backgroundImage.scale.x;
        }
    }

    //-------------------------------------------------------------------------
    _initBoard() {
        this._board = new Board(this._gameState, this._boardGroup);
    }

    //-------------------------------------------------------------------------
    _initButton(position: any, namePrefix: string, onPressedCallback): Button {
        const button = new Button(
            this._gameState,
            position.x, position.y,
            'graphics',
            onPressedCallback,
            this,
            namePrefix);

        this._buttonsGroup.add(button);
        return(button);
    }

    //-------------------------------------------------------------------------
    _initPlayersUis() {
        this._playersUis = [];

        this._gameState.playersData.forEach(function(playerData, playerIndex) {
            const playerUi = new PlayerUi(this._gameState, playerData, playerIndex,
                this._uiGroup);

            this._playersUis.push(playerUi);
        }, this);
    }

    //-------------------------------------------------------------------------
    _setDisplayObjectsVisibile(displayObjects: PIXI.DisplayObject[], isVisible: boolean) {
        displayObjects.forEach(function(displayObject) {
            if (displayObject) {
                displayObject.visible = isVisible;
            }
        });
    }
}
