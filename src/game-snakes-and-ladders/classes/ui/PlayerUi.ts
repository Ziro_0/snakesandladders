import IGameState from "../../interfaces/IGameState";
import PlayerData from "../data/PlayerData";
import TimerBar from "./TimerBar";
import DiceDisplay from "./DiceDisplay";
import PlayerTurnIndicator from "./PlayerTurnIndicator";
import { AudioEventKey } from "../audio/AudioEventKey";
import TurnTimer from "../TurnTimer";

export default class PlayerUi extends Phaser.Group {
    _gameState: IGameState;

    _timerBar: TimerBar;
    _diceDisplay: DiceDisplay;
    _turnIndicator: PlayerTurnIndicator;
    _turnTimer: TurnTimer;

    _avatarImage: Phaser.Image;
    _winnerIcon: Phaser.Image;

    _playerIndex: number;
    _nextDiceRollValue: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, playerData: PlayerData, playerIndex: number,
    parent?: Phaser.Group, shouldAddToStage?: boolean) {
        super(gameState.game, parent, name, shouldAddToStage);
        this._init(gameState, playerData, playerIndex);
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    destroy() {
        this._turnTimer.dispose();
    }

    //-------------------------------------------------------------------------
    set diceEnabled(value: boolean) {
        this._diceDisplay.enabled = value;
    }

    //-------------------------------------------------------------------------
    static GetFrame(avatarIndex: number): string {
        const RIGHT = 1;
        return(`avatar${Phaser.Utils.pad(avatarIndex.toString(), 2, '0', RIGHT)}`);
    }

    //-------------------------------------------------------------------------
    set isDiceVisible(value: boolean) {
        this._diceDisplay.visible = value;
    }

    //-------------------------------------------------------------------------
    set nextDiceRollValue(value: number) {
        this._nextDiceRollValue = value;
    }

    //-------------------------------------------------------------------------
    get playerIndex(): number {
        return(this._playerIndex);
    }

    //-------------------------------------------------------------------------
    presentTurnIndicator() {
        this._turnIndicator.present();
    }

    //-------------------------------------------------------------------------
    presentWinnerIcon() {
        const jPlayerData = this._gameState.metricsJsonData.players[this._playerIndex];
        const jWinnerIconPosition = jPlayerData.winnerIconPosition;
        this._gameState.game.add.image(
            jWinnerIconPosition.x,
            jWinnerIconPosition.y,
            'graphics', 'winner-icon', this);
    }

    //-------------------------------------------------------------------------
    rollDice(value?: number) {
        value = this._resolveDiceValue(value);
        this._diceDisplay.roll(value);
    }

    //-------------------------------------------------------------------------
    setDiceIdle() {
        this._diceDisplay.setIdle();
    }

    //-------------------------------------------------------------------------
    startTurnTimer(timeMs: number) {
        this._turnTimer.start(timeMs, this._timerBar);
    }

    //-------------------------------------------------------------------------
    stopTurnTimer() {
        this._turnTimer.stop();
    }

    //-------------------------------------------------------------------------
    unpresentTurnIndicator() {
        this._turnIndicator.unpresent();
    }

    //-------------------------------------------------------------------------
    update() {
        if (this._turnTimer.isRunning) {
            this._turnTimer.update();
        }
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _init(gameState: IGameState, playerData: PlayerData, playerIndex: number) {
        this._gameState = gameState;
        this._playerIndex = playerIndex;

        this._turnTimer = new TurnTimer(this._gameState.game);

        const jPlayerData = gameState.metricsJsonData.players[playerIndex];

        this._initAvatar(jPlayerData.avatarPosition, playerData.avatarIndex);
        this._initNameText(jPlayerData.nameTextPositionY, playerData.name);
        this._initTimerBar(jPlayerData.timerBarPosition);
        this._initDice(jPlayerData.dicePosition);
        this._initTurnIndicator(jPlayerData.turnIndicatorPositions);
    }

    //-------------------------------------------------------------------------
    _initAvatar(position: any, avatarIndex: number) {
        const frame = PlayerUi.GetFrame(avatarIndex);
        this._avatarImage = this._gameState.game.add.image(
            position.x, position.y, 'graphics', frame, this);
    }

    //-------------------------------------------------------------------------
    _initDice(dicePosition: any) {
        this._diceDisplay = new DiceDisplay(this._gameState.game,
            dicePosition.x, dicePosition.y, 'graphics');
        this._diceDisplay.visible = false;
        
        this._diceDisplay.onInputUp.add(this._onDiceInputUp, this);
        this._diceDisplay.onRollCompleted.add(this._onDiceRollComplete, this);

        this.add(this._diceDisplay);
    }

    //-------------------------------------------------------------------------
    _initNameText(nameTextPositionY: number, playerName: string) {
        const bmpText = this._gameState.game.add.bitmapText(
            this._avatarImage.centerX,
            nameTextPositionY,
            'open-sans-bold', playerName, undefined, this);

        bmpText.anchor.set(0.5, 0);
    }

    //-------------------------------------------------------------------------
    _initTimerBar(timerBarPosition: any) {
        this._timerBar = new TimerBar(this._gameState.game,
            timerBarPosition.x,
            timerBarPosition.y,
            this,
            'graphics');
        this._timerBar.visible = false;
    }

    //-------------------------------------------------------------------------
    _initTurnIndicator(turnIndicatorPositions) {
        const startPosition = new Phaser.Point(
            turnIndicatorPositions.x, turnIndicatorPositions.yPositions[0]);

        const endPosition = new Phaser.Point(
            turnIndicatorPositions.x, turnIndicatorPositions.yPositions[1]);

        this._turnIndicator = new PlayerTurnIndicator(this._gameState.game,
            startPosition.x, startPosition.y,
            'graphics', startPosition, endPosition, 'turn-player-pointer');
        this._turnIndicator.visible = false;
        
        this.add(this._turnIndicator);
    }

    //-------------------------------------------------------------------------
    _onDiceInputUp() {
        this.unpresentTurnIndicator();
        this._turnTimer.stop();
        this._gameState.playSound(AudioEventKey.DICE);
        this._gameState.onPlayerDiceInputUp(this.playerIndex);
        this._diceDisplay.roll(this._nextDiceRollValue);
    }

    //-------------------------------------------------------------------------
    _onDiceRollComplete(rollValue: number) {
        this._gameState.onPlayerDiceRollComplete(this.playerIndex, rollValue);
    }
    
    //-------------------------------------------------------------------------
    _resolveDiceValue(value: number): number {
        const MAX_MIN_VALUE = 1;
        const MAX_DICE_VALUE = 6;

        if (!value) {
            value = this._nextDiceRollValue;
            if (!value) {
                value = MAX_MIN_VALUE + Math.floor(Math.random() * MAX_DICE_VALUE);
            }
        }

        return(Math.max(MAX_MIN_VALUE, Math.min(MAX_DICE_VALUE, value)));
    }
}
