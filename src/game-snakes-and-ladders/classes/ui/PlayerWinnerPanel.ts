import IGameState from "../../interfaces/IGameState";
import PlayerData from "../data/PlayerData";
import PlayerUi from "./PlayerUi";
import Button from "./Button";

export default class PlayerWinnerPanel extends Phaser.Group {
    _gameState: IGameState;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, playerData: PlayerData, parent: Phaser.Group) {
        super(gameState.game, parent)
        this._gameState = gameState;

        const jWinnerPanelData = this._gameState.metricsJsonData.playerWinnerPanel;

        this._initPanelImage();
        this._initAvatarImage(jWinnerPanelData, playerData);
        this._initWinnerIcon(jWinnerPanelData);
        this._initTexts(jWinnerPanelData, playerData);
        this._initCloseButton(jWinnerPanelData);
    }

    //=========================================================================
    // public
    //=========================================================================

    //=========================================================================
    // private
    //=========================================================================
    
    //-------------------------------------------------------------------------
    _initAvatarImage(jWinnerPanelData: any, playerData: PlayerData) {
        const frame = PlayerUi.GetFrame( playerData.avatarIndex);
        this._gameState.game.add.image(
            jWinnerPanelData.avatarPosition.x,
            jWinnerPanelData.avatarPosition.y,
            'graphics', frame, this);
    }

    //-------------------------------------------------------------------------
    _initCloseButton(jWinnerPanelData: any) {
        const jCloseButton = jWinnerPanelData.closeButton;
        const button = new Button(this._gameState,
            jCloseButton.x,
            jCloseButton.y,
            'graphics',
            this._onCloseButtonPressed, this,
            'game_button_close');
        button.scale.set(jCloseButton.scale);
        this.add(button);
    }

    //-------------------------------------------------------------------------
    _initHeaderText(jWinnerPanelData: any) {
        const jHeaderText = jWinnerPanelData.headerText;
        const bmpText = this._gameState.game.add.bitmapText(
            jHeaderText.x,
            jHeaderText.y,
            'open-sans-bold',
            jHeaderText.text,
            jHeaderText.size,
            this);
        bmpText.anchor.set(0.5, 0);
        bmpText.tint = Phaser.Color.hexToRGB(jHeaderText.color);
    }

    //-------------------------------------------------------------------------
    _initPanelImage() {
        const image = this._gameState.game.add.image(0, 0, 'graphics', 'message-panel', this);
        image.anchor.set(0.5);
    }

    //-------------------------------------------------------------------------
    _initPlayerNameText(jWinnerPanelData: any, name: string) {
        const jPlayerNameText = jWinnerPanelData.playerNameText;
        const bmpText = this._gameState.game.add.bitmapText(
            jPlayerNameText.x,
            jPlayerNameText.y,
            'open-sans-bold',
            name,
            jPlayerNameText.size,
            this);
        bmpText.anchor.set(0.5, 0);
        bmpText.tint = Phaser.Color.hexToRGB(jPlayerNameText.color);
    }

    //-------------------------------------------------------------------------
    _initTexts(jWinnerPanelData: any, playerData: PlayerData) {
        this._initHeaderText(jWinnerPanelData);
        this._initPlayerNameText(jWinnerPanelData, playerData.name);
    }

    //-------------------------------------------------------------------------
    _initWinnerIcon(jWinnerPanelData: any) {
        const jWinnerIconPosition = jWinnerPanelData.winnerIconPosition;
        this._gameState.game.add.image(
            jWinnerIconPosition.x,
            jWinnerIconPosition.y,
            'graphics',
            'winner-icon',
            this);
    }

    //-------------------------------------------------------------------------
    _onCloseButtonPressed() {
        this._gameState.callListenerMap('gameClosed');
    }
}
