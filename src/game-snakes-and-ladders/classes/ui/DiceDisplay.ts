import { irandomRange } from '../../util/util';

export default class DiceDisplay extends Phaser.Image {
    onInputUp: Phaser.Signal;
    onRollCompleted: Phaser.Signal;
    
    valueAfterRoll: number;

    _diceRollTextures: string[];
    
    _timer: Phaser.Timer;
    _timerEvent: Phaser.TimerEvent;

    _idleTexture: string;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(game: Phaser.Game, x: number, y: number, key: string,
    frame?: string) {
        const diceRollTextures = [
            'dice-1',
            'dice-2',
            'dice-3',
            'dice-4',
            'dice-5',
            'dice-6'
        ];

        super(game, x, y, key,
            frame ? frame : Phaser.ArrayUtils.getRandomItem(diceRollTextures));

        this.anchor.set(0.5);
        
        this._idleTexture = 'dice-idle';
        this._diceRollTextures = diceRollTextures;

        this._initTimer();

        this.inputEnabled = true;
        this.input.useHandCursor = true;
        this.events.onInputUp.add(this._onInputUp, this);

        this.onInputUp = new Phaser.Signal();
        this.onRollCompleted = new Phaser.Signal();
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    destroy() {
        this._uninitTimer();
        this.onRollCompleted.dispose();
        this.onInputUp.dispose();

        super.destroy();
    }

    //-------------------------------------------------------------------------
    set enabled(value: boolean) {
        this.input.enabled = value;
    }

    //-------------------------------------------------------------------------
    roll(value?: number) {
        if (value === undefined) {
            this.valueAfterRoll = irandomRange(1, 6);
        } else {
            this.valueAfterRoll = value;
        }

        this._startTimerEvent();
    }

    //-------------------------------------------------------------------------
    setIdle() {
        if (this._idleTexture) {
            this.frameName = this._idleTexture;
        }
    }

    //-------------------------------------------------------------------------
    set value(points: number) {
        this.frameName = 'dice-' + points;
        
        if (this._timerEvent) {
            this._timer.remove(this._timerEvent);
            this._timerEvent = null;
        }
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _initTimer() {
        const AUTO_DESTROY = false;
        this._timer = this.game.time.create(AUTO_DESTROY);
        this._timer.onComplete.add(this._onTimerComplete, this);
        this._timer.start();
    }

    //-------------------------------------------------------------------------
    _onInputUp(diceDisplay: DiceDisplay, pointer: Phaser.Point, isOver: boolean) {
        if (!isOver) {
            return;
        }
        
        this.enabled = false;
        this.onInputUp.dispatch();
    }

    //-------------------------------------------------------------------------
    _onTimer() {
        const texture = Phaser.ArrayUtils.getRandomItem(this._diceRollTextures);
        this.frameName = texture;
    }

    //-------------------------------------------------------------------------
    _onTimerComplete() {
        this.value = this.valueAfterRoll;
        this.onRollCompleted.dispatch(this.valueAfterRoll);
    }

    //-------------------------------------------------------------------------
    _startTimerEvent() {
        this._timer.remove(this._timerEvent);
        this._timerEvent = this._timer.repeat(30, 20, this._onTimer, this);
    }

    //-------------------------------------------------------------------------
    _uninitTimer() {
        if (this._timer) {
            this._timer.destroy();
            this._timer = null;
        }
    }
}
