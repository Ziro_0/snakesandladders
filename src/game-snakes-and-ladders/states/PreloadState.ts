export default class PreloadState {
    static readonly _BASE_PATH = 'assets/game-snakes-and-ladders/';
    game: Phaser.Game;
    preloadBar: Phaser.Image;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(game: Phaser.Game) {
        this.game = game;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    create() {
        this.game.state.start('GameState');
    }

    //-------------------------------------------------------------------------
    preload() {
        this.preloadBar = this.game.add.image(
            this.game.world.centerX,
            this.game.world.centerY,
            'preload_bar');
        this.preloadBar.anchor.set(0.5);

        this.game.load.setPreloadSprite(this.preloadBar);

        this._loadJson('audio');
        this._loadJson('metrics');

        this._loadAtlasJsonArray('graphics');

        this._loadFont('open-sans-bold');

        this._loadAudio('sndButtonPress');
        this._loadAudio('sndDice');
        this._loadAudio('sndLadder');
        this._loadAudio('sndNextTurn');
        this._loadAudio('sndPlayerTimeOver');
        this._loadAudio('sndPlayerWins');
        this._loadAudio('sndSnake');
        this._loadAudio('sndTokenMoveStep');
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _loadAtlasJsonArray(key: string) {
        const path = `${PreloadState._BASE_PATH}images/`;
        this.game.load.atlasJSONArray(key,
            `${path}${key}.png`,
            `${path}${key}.json`);
    }

    //-------------------------------------------------------------------------
    _loadAudio(key: string) {
        const path = `${PreloadState._BASE_PATH}audio/`;
        this.game.load.audio(key,
            [
                `${path}${key}.mp3`,
                `${path}${key}.ogg`
            ]);
    };

    //-------------------------------------------------------------------------
    _loadFont(key: string) {
        const path = `${PreloadState._BASE_PATH}images/`;
        this.game.load.bitmapFont('open-sans-bold',
            `${path}${key}.png`,
            `${path}${key}.xml`);
    }

    //-------------------------------------------------------------------------
    _loadJson(key: string) {
        const path = `${PreloadState._BASE_PATH}data/`;
        this.game.load.json(key, `${path}${key}.json`);
    }        
}
