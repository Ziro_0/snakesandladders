import MainView from "../classes/ui/MainView";
import { Game } from "../game";
import PlayerData from "../classes/data/PlayerData";
import IGameState from "../interfaces/IGameState";
import { JsonKey } from "../classes/data/JsonKey";
import AudioPlayer from "../classes/audio/AudioPlayer";
import { AudioEventKey } from "../classes/audio/AudioEventKey";
import PlayerUi from "../classes/ui/PlayerUi";

export default class GameState implements IGameState {
    static readonly _THIS_PLAYER_INDEX = 0;

    game: Game;
    
    _mainView: MainView;

    _audioPlayer: AudioPlayer;

    _wouldPlayerMovePastExit: boolean;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(game: Game) {
        this.game = game;
    }

    //=========================================================================
    // public
    //=========================================================================
    
    //-------------------------------------------------------------------------
    get audioJsonData(): any {
        return(this.game.cache.getJSON('audio'));
    }

    //-------------------------------------------------------------------------
    callListenerMap(key: string, ...args) {
        const gameObject: any = this.game;
        const callback: Function = gameObject.listenerMapping[key];
        if (callback) {
            callback.apply(null, args);
        }
    }

    //-------------------------------------------------------------------------
    create() {
        console.log('Game state [create] started');
        this._init();
        this.callListenerMap('ready', this.game);
    }

    //-------------------------------------------------------------------------
    gatPlayerUi(playerIndex: number): PlayerUi {
        return(this._mainView.getPlayerUi(playerIndex));
    }

    //-------------------------------------------------------------------------
    getPointFromJsonChain(jsonCacheKey: JsonKey,
    propertyNameChains: string[]): Phaser.Point {
        let jData = this.game.cache.getJSON(jsonCacheKey);
        if (!jData || !propertyNameChains) {
            return(null);
        }

        for (let index = 0; index < propertyNameChains.length; index++) {
            const propName = propertyNameChains[index];
            if (propName in jData) {
                jData = jData[propName];
            } else {
                return(null);
            }
        }

        return(new Phaser.Point(jData.x, jData.y));
    }

    //-------------------------------------------------------------------------
    get metricsJsonData(): any {
        return(this.game.cache.getJSON('metrics'));
    }

    //-------------------------------------------------------------------------
    moveTurnPlayer() {
        this._mainView.getPlayerUi(this.turnPlayerIndex).stopTurnTimer();
        this._mainView._board.movePlayerToken(this.turnPlayerIndex);
    }

    //-------------------------------------------------------------------------
    onPlayerDiceInputUp(playerIndex: number) {
        this.callListenerMap('dicePressed', this.game, playerIndex);
    }

    //-------------------------------------------------------------------------
    onPlayerDiceRollComplete(playerIndex: number, rollValue: number) {
        if (this._wouldPlayerMovePastExit) {
            const DELAY = 400;
            const timer = this.game.time.create();
            timer.start();
            timer.add(DELAY, function() {
                this.callListenerMap('playerMoveTokenComplete');
            }, this);
        } else {
            this._mainView._board.movePlayerToken(playerIndex);
        }
    }

    //-------------------------------------------------------------------------
    get playersData(): PlayerData[] {
        return(this.game.homeServer.playersData);
    }

    //-------------------------------------------------------------------------
    playSound(key: AudioEventKey, callback?: Function, context?: any): boolean {
        return(this._audioPlayer.play(key, callback, context))
    }

    //-------------------------------------------------------------------------
    setTurnPlayer(turnPlayerIndex: number, diceRollValue?: number,
    isFirstTurnOfGame?: boolean, wouldPlayerMovePastExit?: boolean) {
        this._wouldPlayerMovePastExit = wouldPlayerMovePastExit;

        const playerUi = this._mainView.getPlayerUi(turnPlayerIndex);
        playerUi.presentTurnIndicator();
        playerUi.nextDiceRollValue = diceRollValue;
        playerUi.setDiceIdle();
        playerUi.isDiceVisible = true;
        playerUi.diceEnabled = true;

        playerUi.startTurnTimer(this.game.config.playerTimePerTurn);

        if (!isFirstTurnOfGame) {
            this.playSound(AudioEventKey.NEXT_TURN);
        }
    }

    //-------------------------------------------------------------------------
    shutdown() {
        this._mainView.dispose();
        this._audioPlayer.dispose();
    }

    //-------------------------------------------------------------------------
    get turnPlayerIndex(): number {
        return(this.game.homeServer.turnPlayerIndex);
    }

    //-------------------------------------------------------------------------
    turnPlayerTimeExpired() {
        this._mainView.turnPlayerTimeExpired();
    }

    //-------------------------------------------------------------------------
    get turnPlayerData(): PlayerData {
        return(this.playersData[this.turnPlayerIndex]);
    }

    //-------------------------------------------------------------------------
    turnPlayerWins() {
        this.playSound(AudioEventKey.PLAYER_WINS);
        this.gatPlayerUi(this.turnPlayerIndex).presentWinnerIcon();
        this._mainView.presentWinnerPanel();
    }

    //=========================================================================
    // private
    //=========================================================================
    
    //-------------------------------------------------------------------------
    _init() {
        this._audioPlayer = new AudioPlayer(this);
        this._initMainView();
    }

    //-------------------------------------------------------------------------
    _initMainView() {
        this._mainView = new MainView(this);
    }
}
